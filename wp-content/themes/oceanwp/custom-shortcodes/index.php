<?php
function shortcode_last_events( $attr ) {
    ob_start();
    get_template_part( './templates/last-events' );
    return ob_get_clean();
}
add_shortcode( 'last_events', 'shortcode_last_events' );

?>