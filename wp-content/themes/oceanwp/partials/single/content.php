<?php
/**
 * Post single content
 *
 * @package OceanWP WordPress theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<?php do_action( 'ocean_before_single_post_content' ); ?>

<div class="entry-content clr"<?php oceanwp_schema_markup( 'entry_content' ); ?>>

<?php if( get_field('дата_начала_мероприятия') ): ?>
<table>
  <tbody>
    <tr>
		<td>Дата и время начала</td>
      	<td><?php echo get_field('дата_начала_мероприятия'); ?></td>
    </tr>
	<tr>
		<td>Дата и время окончания</td>
      	<td><?php echo get_field('дата_окончания_мероприятия'); ?></td>
    </tr>
	<tr>
		<td>Место проведения</td>
      	<td><?php echo get_field('место_проведения'); ?></td>
    </tr>
	<tr>
		<td>Тип мероприятия: </td>
      	<td>

		  <?php $post_tags = get_the_tags();
			if ( $post_tags ) {
				echo $post_tags[0]->name; 
			} ?>
		  
		</td>
    </tr>
	
  </tbody>
</table>
<? endif; ?>
	<?php the_content();

	wp_link_pages( array(
		'before'      => '<div class="page-links">' . __( 'Pages:', 'oceanwp' ),
		'after'       => '</div>',
		'link_before' => '<span class="page-number">',
		'link_after'  => '</span>',
	) ); ?>
</div><!-- .entry -->

<?php do_action( 'ocean_after_single_post_content' ); ?>

