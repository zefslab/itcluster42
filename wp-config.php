<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'cluster' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*(@JS>}pyE@zd4K/3yR-R}@[56ga)d(Sl4{-E<GQB39jQA($BwElG}G Y7AEkMdt' );
define( 'SECURE_AUTH_KEY',  '2@RYOhYAV.()&:gGtV-Oe{(rm! W|5?+EvFiG~/o%a5L}{$MdLyK15Q8#Tj7O|i5' );
define( 'LOGGED_IN_KEY',    'JWC&#c,R_2D{zM_QXmv::OyU$mf<}lO~Kxc;WKp>qv2SW3LYd!OA|2R=cV/*!6.{' );
define( 'NONCE_KEY',        '8Gj]2`3^X)I+?5.[MH_T@]%%m5_FfNH31I,29q8f0op#}TmFi!VvEj4.t/cahT++' );
define( 'AUTH_SALT',        '?t8@H}`tzhp)]bO[LTl^PiG8kaImT4VDx]8jyNlWIDjBBg2}rE4|5B]xbP6dh@$E' );
define( 'SECURE_AUTH_SALT', 'm;~KQq$6l)L5awp. C/#34sl::h@*1$5@xt^ff4Bh#5wNLi%s_t71AdS[5FyOz6I' );
define( 'LOGGED_IN_SALT',   't@30y*KBY%cRnmN~5thPP7@ix/VTGlvi.BJ&t<a?m_<6YifAjj#XGOQ,bv 0/Gx1' );
define( 'NONCE_SALT',       'DvRELn(7&;ep47/s%G+}I7|:j*Ryi,s wGWKd=!wp>hR #-$jQB*1kX>KvhQDjY{' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
